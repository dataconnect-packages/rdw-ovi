# RDW OVI v2, Rijksdienst voor het Wegverkeer

Build in PHP for, retrieving car information from RDW OVI version 2.<br>
To retrieve data, you need to be a registered company @ RDW <iv@rdw.nl> and have purchased RDW certificates.<br>
sidenote: This is not RDW opendata! Dataset displayed can have other information then opendata does.

## Requirements
rdw-ovi can be run with PHP 8.0 and above. Not tested with earlier versions.

## Installation
Can be installed with [Composer](https://getcomposer.org/).

To get the latest version of rdw-ovi use:
```bash
composer require dataconnect/rdw-ovi
```

## Example
```bash
use RdwOvi\RdwOviApiClient;
use RdwOvi\RdwOviException;

$apiUrl = 'https://berichten.rdw.nl/procidv2';
$certificate = __DIR__ . 'placeholder.pen';
$privatekey = __DIR__ . 'placeholder.pen';

$rdwOviApiClient = new RdwOviApiClient($apiUrl, $certificate, $privatekey);

try {
    $licensePlate = 'S801PV';
    $vehicleInfo = $rdwOviApiClient->getVehicleInfo($licensePlate);

    echo "Brand: {$vehicleInfo['brand']}<br>";
    echo "Model: {$vehicleInfo['model']}<br>";
    echo "Color: {$vehicleInfo['color']}<br>";
} catch (RdwOviException $e) {
    echo 'RdwOviException: ' . $e->getMessage();
} catch (\Exception $e) {
    echo 'Exception: ' . $e->getMessage();
}
```
## Support
Want to support our work? A donation is very welcome: https://buymeacoffee.com/dataconnect

## Contact
Please feel free to contact us, to add options if possible!

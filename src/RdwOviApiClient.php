<?php
namespace RdwOvi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RdwOviApiClient
{
    protected $client;
    protected $apiUrl;
    protected $certificate;
    protected $privatekey;

    public function __construct($apiUrl, $certificate, $privatekey)
    {
        $this->apiUrl = $apiUrl;
        $this->client = new Client();
        $this->certificate = $certificate;
        $this->privatekey = $privatekey;
    }

    public function getVehicleInfo($licensePlate)
    {
        $url = $this->apiUrl;

        try {
            $client = new Client([
                'base_uri' => $url,
                'ssl_key' => $this->privatekey,
                'cert' => $this->certificate,
                'verify' => false
            ]);

            $response = $client->post('', [
                'headers' => [
                    'Content-Type' => 'application/xml',
                    'Accept' => 'application/xml',
                ],
                'body' => '<?xml version="1.0" encoding="UTF-8"?>
                    <OPENB-VRTG-INFO>
                        <ALG-GEG>
                            <PROC-IDENT>2336</PROC-IDENT>
                            <PROC-FUNC>1</PROC-FUNC>
                            <STAT-RIP>1</STAT-RIP>
                            <OPT-STAT-TEKST>1</OPT-STAT-TEKST>
                            <INFO-GEBR>X</INFO-GEBR>
                        </ALG-GEG>
                        <KENT-GEG>
                            <KENTEKEN>' . $licensePlate . '</KENTEKEN>
                        </KENT-GEG>
                    </OPENB-VRTG-INFO>',
            ]);

            $responseBody = $response->getBody()->getContents();
            $xml = simplexml_load_string($responseBody);

            $vehicleInfo = [
              'licensePlate' => (string) $xml->{'KENT-GEG'}->{'KENTEKEN'},
              'vehicleType' => (string) $xml->{'VRTG-STAND-GEG'}->{'SOORT-AAND-OMS'},
              'brandCode' => (string) $xml->{'VRTG-STAND-GEG'}->{'MERK-CODE-V'},
              'brand' => (string) $xml->{'VRTG-STAND-GEG'}->{'MERK-BESCHR'},
              'model' => (string) $xml->{'VRTG-STAND-GEG'}->{'TYPE-BESCHR-VTG'},
              'variant' => (string) $xml->{'VRTG-STAND-GEG'}->{'INRICHT-OMS'},
              'doors' => (string) $xml->{'VRTG-STAND-GEG'}->{'AANT-DEUREN'},
              'seats' => (string) $xml->{'VRTG-STAND-GEG'}->{'AANT-ZITPL'},
              'color' => (string) $xml->{'VRTG-STAND-GEG'}->{'ACT-KLEUR-1'},
              'mainFuel' => (string) $xml->{'VRTG-STAND-GEG'}->{'BRANDSTOF-OMS'},
              'alternativeFuel' => (string) $xml->{'VRTG-STAND-GEG'}->{'BRANDSTOF-2-OMS'},
              'firstRegistration' => (string) $xml->{'VRTG-STAND-GEG'}->{'EERSTE-INS-DAT'},
              'firstAdmission' => (string) $xml->{'VRTG-STAND-GEG'}->{'EERSTE-TOEL-DAT'},
              'dateOwnership' => (string) $xml->{'VRTG-STAND-GEG'}->{'REG-DAT-AANSPR'},
              'timeOwnership' => (string) $xml->{'VRTG-STAND-GEG'}->{'REG-TYD-AANSPR'},
              'apkExpiration' => (string) $xml->{'VRTG-STAND-GEG'}->{'VERV-DAT-KEUR'},
              'insuranceStatus' => (string) $xml->{'VRTG-STAND-GEG'}->{'VERZEKERD-IND'},
              'wokStatus' => (string) $xml->{'VRTG-STAND-GEG'}->{'STAT-WOK-IND'},
              'listPrice' => (string) $xml->{'BPM-GEG'}->{'CATALOGUS-PRYS'},
              'bpmAmount' => (string) $xml->{'BPM-GEG'}->{'BPM-BEDRAG'},
              'mileageYear' => (string) $xml->{'TEL-STAND-GEG'}->{'REG-JAAR-TEL-ST'},
              'mileageStatus' => (string) $xml->{'TEL-STAND-GEG'}->{'TRENDINF-KL-OMS'},
              'mileageInfo' => (string) $xml->{'TEL-STAND-GEG'}->{'TOEL-TRENDINFO'}
            ];

            return $vehicleInfo;
        } catch (GuzzleException $e) {
            throw new RdwOviException('Error occurred while accessing the RDW OVI API: ' . $e->getMessage());
        }
    }
}
?>
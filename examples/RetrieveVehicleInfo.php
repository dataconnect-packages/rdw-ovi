<?php
require __DIR__ . '/vendor/autoload.php'; // custom path name to autoload

use RdwOvi\RdwOviApiClient;
use RdwOvi\RdwOviException;

$apiUrl = 'https://berichten.rdw.nl/procidv2';
$certificate = __DIR__ . '/certificate.pem';
$privatekey = __DIR__ . '/privatekey.pem';

$rdwOviApiClient = new RdwOviApiClient($apiUrl, $certificate, $privatekey);

try {
    $licensePlate = 'S801PV';
    $vehicleInfo = $rdwOviApiClient->getVehicleInfo($licensePlate);

    echo "Brand: {$vehicleInfo['brand']}<br>";
    echo "Model: {$vehicleInfo['model']}<br>";
    echo "Color: {$vehicleInfo['color']}<br>";
} catch (RdwOviException $e) {
    echo 'RdwOviException: ' . $e->getMessage();
} catch (\Exception $e) {
    echo 'Exception: ' . $e->getMessage();
}
?>